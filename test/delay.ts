import { AsyncTest, Expect, TestFixture } from 'alsatian';
import delay from '../src/lib/delay';

@TestFixture('delay')
export class DelayTests {
  @AsyncTest('Test async not throw')
  public async notThrowTest(): Promise<void> {
    Expect(async () => await delay(10)).not.toThrow();
  }
  
  @AsyncTest('Test delay at least correct number of ms')
  public async delayCorrectMs(): Promise<void> {
    const start: number = Date.now();
    await delay(10);
    Expect(Date.now() - start).toBeGreaterThan(9);
  }
}
