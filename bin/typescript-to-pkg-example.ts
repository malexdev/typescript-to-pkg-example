import app from '../src';

async function main(): Promise<void> {
  const [ , , address = 'https://www.google.com' ]: string[] = process.argv;
  console.log(`Fetching status code from URL ${address}`);

  const statusText: string = await app(address);
  console.log(`Result: ${statusText}`);
}

main().catch(({stack}: {stack: string}) => {
  console.error(`Fatal error: `, stack);
});
