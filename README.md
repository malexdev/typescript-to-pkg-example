# An example node project that compiles from TypeScript to native (via pkg) inside VS Code

# System Dependencies

- Yarn
- Node v7.6.0 or greater.

# How to Use

- With VS Code:
  - To debug, just press F5.
  - To build:
    - Press Ctrl+Shift+P (Cmd+Shift+P on MacOS).
    - Type "run task" and select "Tasks: Run Task".
    - Select "build-pkg-`<platform>`", where `<platform>` is the OS to target.

- Without VS Code:
  - To debug, run `yarn build-ts` and then run `node src`.
  - To build, run `yarn build-<platform>`, where `<platform>` is the OS to target.