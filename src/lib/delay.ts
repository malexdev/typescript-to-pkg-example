/**
 * Delay for the given number of milliseconds
 */
export default async function delay(ms: number = 0): Promise<void> {
  await new Promise((resolve: () => void) => setTimeout(resolve, ms));
}
