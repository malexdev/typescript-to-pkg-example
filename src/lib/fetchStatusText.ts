import fetch, { Response } from 'node-fetch';

/**
 * Fetch the status text returned from a server
 */
export default async function fetchStatusText(url: string): Promise<string> {
  const result: Response = await fetch(url);
  return result.statusText;
}
