import delay from './lib/delay';
import fetchStatusText from './lib/fetchStatusText';

/**
 * The main entry point for the program
 */
export default async function main(address: string): Promise<string> {
  await delay(100);
  return await fetchStatusText(address);
}
